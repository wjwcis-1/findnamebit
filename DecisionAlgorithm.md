# Decision Algorithm #

1. inputs:
   1. Test name
   1. Test body
   1. threshold value

1. If test name violates Rule 6 
   1. return **True**
   
1. If test name violates Rule 3 
   1. Add to the set of violations
   1. continue
   
1. If test name violates Rule 4
   1. Add to the set of violations
   1. continue
   
1. If test name violates Rule 5-1
   1. Add to the set of violations
   1. continue
   
1. If test name violates Rule 2-1
   1. Add to the set of violations
   1. continue
   
1. If test name violates Rule 2-2
   1. Add to the set of violations
   1. continue
   
1. If test name violates Rule 5-2
   1. Add to the set of violations
   1. continue
   
1. If test name violates Rule 5-3
   1. Add to the set of violations
   1. continue
   
1. If test name violates Rule 1
   1. return **True**
   
1. If the number of violations in the set >= threshold
   1. return **True**
   1. Else return **False**
1. If the threshold is too high and no results are presented
   1. Manually reduce the threshold
   1. Rerun the plugin