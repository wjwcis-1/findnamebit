# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Jianwei Wu Research for detecting "bad" test names

### How do I get set up? ###

* Create a new Plugin project in IntelliJ
* Clone the repo from bitbucket
* Copy the codes in the cloned repo to your new project
* Add libraries to the project: CoreNLP and other dependencies
* Reset the configurations of the project, and change -Xms to 2048m
* Run as IntelliJ Plugin


### Who do I talk to? ###

Prof.James Clause

### Integration ###

* Bllip Parser: Use Stanford CoreNLP's CharniakParser to implement Bllip parser.


* POSSE: Send commands to console and execute them.


* TestStereotype: Import the project as a module in the plugin (require module dependencies of TestStereotype).
