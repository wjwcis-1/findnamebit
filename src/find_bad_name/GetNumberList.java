package find_bad_name;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static find_bad_name.GetR2R5.ToLower;

public class GetNumberList {
    public String[] NumList(){
        // get the numbers
        // exclude: width height length depth measure
        // get the ones which numbers are not in their body
        List<String> NumList = new ArrayList<>();
        Project p = ProjectManager.getInstance().getOpenProjects()[0];
        final Collection<VirtualFile> all_files = FileTypeIndex.getFiles(JavaFileType.INSTANCE,GlobalSearchScope.projectScope(p));
        for (VirtualFile virtualFile : all_files) {
            PsiFile file = PsiManager.getInstance(p).findFile(virtualFile);
            if (file instanceof PsiJavaFile &&
                    (file.getName().contains("Test")
                            || file.getName().contains("test"))) {
                for (PsiClass psiClass : ((PsiJavaFile) file).getClasses()) {
                    for (PsiMethod me : psiClass.getAllMethods()) {
                        if (me.getName().contains("test")
                                || me.getName().contains("Test")) {
                            try {
                                String[] name_into_parts = me.getName().split("(?=\\p{Upper})");
                                String body = me.getBody().getText();
                                String[] list = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
                                //Expand the list
                                for (int i = 0; i < name_into_parts.length; i++) {
                                    for (String part : list) {
                                        if(name_into_parts[i].contains(part) && !body.contains(name_into_parts[i])
                                                && !body.contains(ToLower(name_into_parts[i]))){
                                            NumList.add(name_into_parts[i]);
                                        }
                                    }
                                }
                            }
                            catch (NullPointerException npe){
                                System.out.println("Fail to Get Num list!");
                            }
                            for (String num : NumList){
                                System.out.println("NumList: " + num +"\n");
                            }
                        }
                    }
                }
            }
        }
        String[] number_list = new String[NumList.size()];
        number_list = NumList.toArray(number_list);
        return number_list;
    }
}
