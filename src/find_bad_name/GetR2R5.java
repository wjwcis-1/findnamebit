package find_bad_name;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import java.util.*;

public class GetR2R5 {
    public static String ToLower(String piece) {
        String piece_out = piece.substring(0, 1).toLowerCase() + piece.substring(1);
        return piece_out;
    }
    public static String sub_string_pos(String piece){
        return ToLower(piece.substring(0, piece.indexOf('/')));
    }

    public Set<String> getDifference(String test_name, String test_body) {
        Set<String> threshold_counter = new LinkedHashSet<>();
        //Configure threshold_counter
        String test_name_toSentence = test_name.replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
        System.out.println("Parsed testname:"+test_name_toSentence);
        Properties prop = new Properties();
        prop.setProperty("annotators", "tokenize, ssplit, pos, lemma, depparse");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(prop);
        Annotation a1 = new Annotation(test_name_toSentence);
        pipeline.annotate(a1);
        List<CoreMap> ss = a1.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap s : ss) {
            for (CoreLabel token : s.get(CoreAnnotations.TokensAnnotation.class)) {
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                List<String> lemma = new LinkedList<>();
                lemma.add(token.get(CoreAnnotations.LemmaAnnotation.class));
                //Corenlp change the word into its original form
                String first_word = lemma.get(0);
                String lower = ToLower(first_word);
                if (test_body.contains(lower) && !ToLower(word).equals(lower)) {
                    threshold_counter.add("--testname:" + test_name + "-> rule 5-1 test body contains word in original form->" + lower);
                }
                else {
                    continue;
                }
                //rule 5 word in different form (the original verb form)
            }
        }

        Annotation name = new Annotation(test_name_toSentence);
        Properties prop1 = new Properties();
        prop1.setProperty("annotators", "tokenize, ssplit, pos, lemma, depparse");
        StanfordCoreNLP pp1 = new StanfordCoreNLP(prop1);
        pp1.annotate(name);
        List<CoreMap> ses = name.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap se : ses){
            SemanticGraph sg = se.get(SemanticGraphCoreAnnotations.BasicDependenciesAnnotation.class);
            //System.out.println("Semantic:  " + sg.toDotFormat());
            String sg_text = sg.getFirstRoot().toString();
            if(sg_text.contains("VB")
                    || sg_text.contains("VBZ")
                    || sg_text.contains("VBN")
                    || sg_text.contains("VBD")
                    || sg_text.contains("VBG")
                    || sg_text.contains("VBP"))
            {
                if (test_body.contains(sg_text.substring(0, sg_text.indexOf('/')))
                        || test_body.contains(ToLower(sg_text.substring(0, sg_text.indexOf('/'))))) {
                    continue;
                }
                else {
                    //System.out.println("FirstRoot:" + sg_text);
                    threshold_counter.add("--testname:" + test_name + "-> rule 2-1 lack of verbs/action words->" + ToLower(sg_text.substring(0, sg_text.indexOf('/'))));
                }
            }
            //For finding whether the found root is a verb or not and add counts
            else{
                Set<IndexedWord> nodes = sg.vertexSet();
                Iterator<IndexedWord> iterator = nodes.iterator();
                while (iterator.hasNext()) {
                    String next = iterator.next().toString();
                    System.out.println("Semantic Graph: " + next);
                    if (next.contains("VB") || next.contains("VBZ") ||
                            next.contains("VBN") || next.contains("VBD") ||
                            next.contains("VBG") || next.contains("VBP")) {
                        if (test_body.contains(next.substring(0, next.indexOf('/'))) ||
                                test_body.contains(ToLower(next.substring(0, next.indexOf('/'))))) {
                            continue;
                        }
                        else{
                            //System.out.println("NewRoot:" + next);
                            threshold_counter.add("--testname:" + test_name + "-> rule 2-2 lack of verbs/action words->" + ToLower(next.substring(0, next.indexOf('/'))));
                        }
                    }
                }
                //rule 2 lack of verbs/action words
                //Traverse all vertices in the SemanticGraph until find the first verb
            }
            SemanticGraph semanticGraph_rule5_2 = se.get(SemanticGraphCoreAnnotations.BasicDependenciesAnnotation.class);
            String first_root = semanticGraph_rule5_2.getFirstRoot().toString();
            WordNetSynonyms wordNetSynonyms = new WordNetSynonyms();
            if (first_root.contains("VB")
                    || first_root.contains("VBZ")
                    || first_root.contains("VBN")
                    || first_root.contains("VBD")
                    || first_root.contains("VBG")
                    || first_root.contains("VBP")){
                System.out.println("sub_string_pos_first_root: " + sub_string_pos(first_root));
                for (String verbs : wordNetSynonyms.GetSynonyms_Verb(sub_string_pos(first_root))){
                    if (test_body.contains(verbs)){
                        System.out.println("crucial verb: " + verbs);
                        threshold_counter.add("--testname:" + test_name + "-> rule 5-2 test body contains synonyms of crucial verb->" + verbs);
                    }
                }
            }
            if (first_root.contains("NN")){
                System.out.println("sub_string_pos: " + sub_string_pos(first_root));
                for (String nouns : wordNetSynonyms.GetSynonyms_Noun(sub_string_pos(first_root))){
                    if (test_body.contains(nouns)){
                        System.out.println("crucial noun: " + nouns);
                        threshold_counter.add("--testname:" + test_name + "-> rule 5-3 test body contains synonyms of crucial noun->" + nouns);
                    }
                }
            }
        }
        //Stanford dependency parser
        return threshold_counter;
    }
    //Stanford CoreNLP for rule 2 and rule 5 for stats.txt file
}
