package find_bad_name;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaClass;
import com.thoughtworks.qdox.model.JavaMethod;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.IndexedWord;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.*;
import org.supercsv.prefs.CsvPreference;
import java.io.FileWriter;
import java.io.StringReader;
import java.util.*;

import static find_bad_name.GetR2R5.ToLower;
import static find_bad_name.GetR2R5.sub_string_pos;

public class ParseJavaCodeAndWriteCSV {
    public Set<String> getDifference_1(String test_name, String test_body) {
        Set<String> threshold_counter = new LinkedHashSet<>();
        String test_name_toSentence = test_name.replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
        System.out.println("Parsed testname:"+test_name_toSentence);
        Properties prop = new Properties();
        prop.setProperty("annotators", "tokenize, ssplit, pos, lemma, depparse");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(prop);
        Annotation a1 = new Annotation(test_name_toSentence);
        pipeline.annotate(a1);
        List<CoreMap> ss = a1.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap s : ss) {
            for (CoreLabel token : s.get(CoreAnnotations.TokensAnnotation.class)) {
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                List<String> lemma = new LinkedList<>();
                lemma.add(token.get(CoreAnnotations.LemmaAnnotation.class));
                //corenlp change the word into its original form
                String first_word = lemma.get(0);
                String lower = ToLower(first_word);
                //System.out.println("Get1_CoreNLP Lemma: "+ first_word + "/" + lower + " Word: "+ word);
                if (test_body.contains(lower)&&!ToLower(word).equals(lower)) {
                    threshold_counter.add("rule 5-1 test body contains word in original form->" + lower);
                }
                else{
                    continue;
                }
                //rule 6 word in different form (the original verb form)
            }
        }
        Annotation name = new Annotation(test_name_toSentence);
        Properties prop1 = new Properties();
        prop1.setProperty("annotators", "tokenize, ssplit, pos, lemma, depparse");
        StanfordCoreNLP pp1 = new StanfordCoreNLP(prop1);
        pp1.annotate(name);
        List<CoreMap> ses = name.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap se : ses){
            SemanticGraph sg = se.get(SemanticGraphCoreAnnotations.BasicDependenciesAnnotation.class);
            String sg_text = sg.getFirstRoot().toString();
            if(sg_text.contains("VB")
                    || sg_text.contains("VBZ")
                    || sg_text.contains("VBN")
                    || sg_text.contains("VBD")
                    || sg_text.contains("VBG")
                    || sg_text.contains("VBP"))
            {
                if (test_body.contains(sg_text.substring(0, sg_text.indexOf('/'))) ||
                        test_body.contains(ToLower(sg_text.substring(0, sg_text.indexOf('/'))))) {
                    continue;
                }
                else {
                    threshold_counter.add("rule 2-1 lack of verbs/action words->" + ToLower(sg_text.substring(0, sg_text.indexOf('/'))));
                }
            }
            //For finding whether the found root is a verb or not and add counts
            else{
                Set<IndexedWord> nodes = sg.vertexSet();
                Iterator<IndexedWord> iterator = nodes.iterator();
                while (iterator.hasNext()) {
                    String next = iterator.next().toString();
                    //System.out.println("semantic iterator:" + next);
                    if (next.contains("VB") || next.contains("VBZ") ||
                            next.contains("VBN") || next.contains("VBD") ||
                            next.contains("VBG") || next.contains("VBP")) {
                        if (test_body.contains(next.substring(0, next.indexOf('/'))) ||
                                test_body.contains(ToLower(next.substring(0, next.indexOf('/'))))) {
                            continue;
                        }
                        else{
                            //System.out.println("NewRoot:" + next);
                            threshold_counter.add("rule 2-2 lack of verbs/action words->" + ToLower(next.substring(0, next.indexOf('/'))));
                        }
                    }
                }
                //rule 2 lack of verbs/action words
                //Traverse all vertices in the SemanticGraph until find the first verb
            }
            SemanticGraph semanticGraph_rule5_2 = se.get(SemanticGraphCoreAnnotations.BasicDependenciesAnnotation.class);
            String first_root = semanticGraph_rule5_2.getFirstRoot().toString();
            WordNetSynonyms wordNetSynonyms = new WordNetSynonyms();
            System.out.println("first-root" + first_root);
            if (first_root.contains("VB")
                    || first_root.contains("VBZ")
                    || first_root.contains("VBN")
                    || first_root.contains("VBD")
                    || first_root.contains("VBG")
                    || first_root.contains("VBP")){
                System.out.println("sub_string_pos: " + sub_string_pos(first_root));
                for (String verbs : wordNetSynonyms.GetSynonyms_Verb(sub_string_pos(first_root))){
                    if (test_body.contains(verbs)){
                        threshold_counter.add("rule 5-2 test body contains synonyms of crucial verb->" + verbs);
                    }
                }
            }
            if (first_root.contains("NN")){
                System.out.println("sub_string_pos_first_root: " + sub_string_pos(first_root));
                for (String nouns : wordNetSynonyms.GetSynonyms_Noun(sub_string_pos(first_root))){
                    if (test_body.contains(nouns)){
                        threshold_counter.add("rule 5-3 test body contains synonyms of crucial noun->" + nouns);
                    }
                }
            }
        }
        //Stanford dependency parser
        return threshold_counter;
    }
    //Stanford CoreNLP for rule 2 and rule 5 for all_tests.csv file
    private static CellProcessor[] getProcessors() {

        final CellProcessor[] processors = new CellProcessor[] {
                new NotNull(),
                new NotNull(),
                new NotNull()
        };

        return processors;
    }
    public void writeResult(){
        Project project = ProjectManager.getInstance().getOpenProjects()[0];
        JavaProjectBuilder javaProjectBuilder = new JavaProjectBuilder();
        GlobalSearchScope searchScope = GlobalSearchScope.projectScope(project);
        final Collection<VirtualFile> all_files = FileTypeIndex.getFiles(JavaFileType.INSTANCE, searchScope);
        ICsvBeanWriter beanWriter = null;
        //Get files
        try {
            //Todo: If you are using project from Bitbucket Please change the path below
            beanWriter = new CsvBeanWriter(
                    new FileWriter
                            ("/Users/wujianwei/Desktop/FindName/Output/all_tests.csv",true),
                    CsvPreference.STANDARD_PREFERENCE);
            final String[] header = new String[]
                    {"TestName","TestBody","VioOFRules"};
            beanWriter.writeHeader(header);
            final CellProcessor[] processors = getProcessors();
            final List<Violation> violations = new ArrayList<>();
            //Set up writer

            for (VirtualFile virtualFile : all_files) {
                PsiFile file = PsiManager.getInstance(project).findFile(virtualFile);
                if (file instanceof PsiJavaFile &&
                        (file.getName().contains("Test") || file.getName().contains("test"))) {
                    //Find all test files
                    javaProjectBuilder.addSource(new StringReader(file.getText().toString()));
                    Collection<JavaClass> all_class = javaProjectBuilder.getClasses();
                    for (JavaClass javaClass : all_class) {
                        if (javaClass.getName().contains("Test")
                                || javaClass.getName().contains("test")) {
                            List<JavaMethod> javaMethod = javaClass.getMethods();
                            for (int i = 0; i < javaMethod.size(); i++) {
                                if (javaMethod.get(i).getName().contains("test")
                                        || javaMethod.get(i).getName().contains("Test")) {

                                    String each_name = javaMethod.get(i).getName().toString();
                                    each_name = each_name.replaceAll("[\\t\\n\\r]", "");
                                    each_name = each_name.replaceAll("\\p{Z}", "");
                                    each_name = each_name.replaceAll("\"", "");
                                    //Name

                                    String each_body = javaMethod.get(i).getSourceCode().toString();
                                    each_body = each_body.replaceAll("[\\t\\n\\r]", "");
                                    each_body = each_body.replaceAll("\\p{Z}", "");
                                    each_body = each_body.replaceAll("\"", "");
                                    //Body
                                    System.out.println("TestName-->" + javaMethod.get(i).getName() + ";;;");

                                    GetAllRules getAllRules = new GetAllRules();
                                    //String rule_result = getAllRules.getAllOtherDifference(
                                            //javaMethod.get(i).getName());
                                    String rule_result = null;
                                    rule_result = rule_result.replaceAll("[\\t\\n\\r]", "");
                                    rule_result = rule_result.replaceAll("\\p{Z}", "");
                                    rule_result = rule_result.replaceAll("\"", "");
                                    //Rule

                                    System.out.println("method name: " + each_name);
                                    System.out.println("method body: " + each_body);
                                    System.out.println("parsed result: " + rule_result);
                                    //Get each column
                                    if (!rule_result.equals("Norulesappliedforthistestname!")) {
                                        System.out.println("Have Vio!");
                                        violations.add(new Violation
                                                (each_name, each_body, rule_result));
                                    }
                                    //New instance from novel class-Violation
                                }
                            }
                        }
                    }
                }
                else{
                    continue;
                }
            }
            //Add all to Violations list
            List<Violation> violation_no_dup = new ArrayList<>();
            for (Violation v0 : violations) {
                boolean isFound = false;
                for (Violation v : violation_no_dup) {
                    if (v.getTestName().equals(v0.getTestName())
                            && v.getTestBody().equals(v0.getTestBody())
                            && v.getVioOFRules().equals(v0.getVioOFRules())) {
                        isFound = true;
                        break;
                    } else {
                        continue;
                    }
                }
                if (!isFound) {
                    violation_no_dup.add(v0);
                } else {
                    continue;
                }
            }
            //Remove duplicates in violations
            for (Violation v1 : violation_no_dup) {
                beanWriter.write(v1, header, processors);
            }
            //Write to CSV file
        }
        catch (Exception e1) {
            e1.printStackTrace();
        }
        finally {
            try{
                beanWriter.close();
            }
            catch (Exception e2){
                e2.printStackTrace();
            }
        }
    }

}
