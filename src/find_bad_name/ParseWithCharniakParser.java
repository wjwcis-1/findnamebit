package find_bad_name;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.parser.charniak.CharniakParser;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.WordTokenFactory;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.Trees;
import edu.stanford.nlp.util.ScoredObject;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ParseWithCharniakParser {
    //The followings are methods to parse
    public void parseByCharniakParser(){
        try {
            File stats = new File("/Users/wujianwei/Desktop/FindName/Output/stats.txt");
            FileWriter fw = new FileWriter(stats, true);
            BufferedWriter output = new BufferedWriter(fw);
            Project p = ProjectManager.getInstance().getOpenProjects()[0];
            final Collection<VirtualFile> all_files = FileTypeIndex.getFiles(JavaFileType.INSTANCE,GlobalSearchScope.projectScope(p));
            String parser_dir = "/Users/wujianwei/Desktop/bllip-parser";
            String parser_executable = "/Users/wujianwei/Desktop/bllip-parser/parse-50best.sh";
            CharniakParser charniakParser1 = new CharniakParser(parser_dir, parser_executable);
            //Setup for charniak parser

            for (VirtualFile virtualFile : all_files) {
                PsiFile file = PsiManager.getInstance(p).findFile(virtualFile);
                if (file instanceof PsiJavaFile &&
                        (file.getName().contains("Test")
                                || file.getName().contains("test"))) {
                    for (PsiClass psiClass : ((PsiJavaFile) file).getClasses()) {
                        //System.out.println("class name: " + psiClass.getName());
                        for (PsiMethod test : psiClass.getAllMethods()) {
                            //System.out.println("test name: " + test.getName());
                            SplitTestName splitTestName1 = new SplitTestName();
                            String single_test_name = splitTestName1.nameSplitToString(test.getName());
                            output.write("<s>" + single_test_name + " </s>\n");
                            //Write output file for One-time Parsing

                            PTBTokenizer<Word> ptbTokenizer1 = new PTBTokenizer<>(
                                    new StringReader(single_test_name), new WordTokenFactory(), "");
                            List<Word> name1 = new ArrayList<>();
                            for (Word token; ptbTokenizer1.hasNext(); ) {
                                token = ptbTokenizer1.next();
                                name1.add(token);
                                output.write("tokens: " + token.toString() +"\n");
                            }
                            //Setup each test name for individual parsing
                            //charniakParser1.printSentence(name1,"/Users/wujianwei/Desktop/FindName/Output/cur_sentence.txt");
                            //Print current sentence
                            List<ScoredObject<Tree>> trees_20 = charniakParser1.getKBestParses(name1, 3);
                            for (int i = 0; i < 3; i++){
                                output.write("No." + i +"\n");
                                output.write("Parsed score: " + trees_20.get(i).score() + "\n");
                                output.write("Parsed tree: " + trees_20.get(i).object().toString() + "\n");
                                Tree each_tree = Trees.readTree(trees_20.get(i).object().toString());
                                each_tree.indentedListPrint();
                                output.write("Tree size: "+ each_tree.size() +
                                        " value:" + each_tree.value() +
                                        " nodeString: " + each_tree.nodeString() + "\n");
                                for (Tree a : each_tree.getLeaves()
                                     ) {
                                    output.write("Leaves: "+a.toString() + "\n");
                                }
                            }
                            output.write("End for this test name" + "\n");
                            //Split the test name into words and get tree after parsing
                        }
                    }
                }
            }
            output.close();
            //Complete the stats file
            charniakParser1.runCharniak(
                    50,
                    "/Users/wujianwei/Desktop/FindName/Output/stats.txt",
                    "/Users/wujianwei/Desktop/FindName/Output/charniakResults.txt",
                    "/Users/wujianwei/Desktop/FindName/Output/err.txt");
            //Parse all test names from a single file
        }
        catch (IOException ioe1){
            ioe1.fillInStackTrace();
        }

    }

    public List<String> returnResultsByCharniakParser(){
        //TODO: Future works

        return null;
    }

}
