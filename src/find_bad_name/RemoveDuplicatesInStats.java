package find_bad_name;

import java.io.*;
import java.util.LinkedHashSet;
import java.util.Set;

public class RemoveDuplicatesInStats {
    public void removeDup() throws IOException{
        BufferedReader bufferedReader = new BufferedReader(new FileReader("/Users/wujianwei/Desktop/FindName/Output/stats.txt"));
        Set<String> all = new LinkedHashSet<>();
        String each_line;
        while ((each_line = bufferedReader.readLine()) != null) {
            all.add(each_line);
        }
        bufferedReader.close();
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("/Users/wujianwei/Desktop/FindName/Output/stats.txt"));
        for (String unique : all) {
            bufferedWriter.write(unique);
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
    }

}
