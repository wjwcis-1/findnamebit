package find_bad_name;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.newvfs.impl.VirtualFileImpl;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiJavaFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.testFramework.LightVirtualFile;
import edu.wm.constants.ConfigConstants;
import edu.wm.core.TestStereotypeAnalyzer;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import static org.apache.commons.io.FileUtils.copyFileToDirectory;


public class ToFindBadNames extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent e) {
        //TODO: insert action logic here
        if (!(e.getData(LangDataKeys.PSI_FILE) instanceof PsiJavaFile)) {
            return;
        }
        ParseBodyWithTestStereotype parseBodyWithTestStereotype1 = new ParseBodyWithTestStereotype();
        parseBodyWithTestStereotype1.runTestStereotypeForAllTestFiles();
        //Run TestStereotype

        ParseWithPOSSE parseWithPOSSE1;
        parseWithPOSSE1 = new ParseWithPOSSE();
        parseWithPOSSE1.runPOSSEWithMethodCommandThenPrintParsedTestName();
        //POSSE Ready

        GetAllRules getAllRules = new GetAllRules();
        getAllRules.threshold = 1;
        getAllRules.print_test_body = true;
        //Change threshold here & Exclude Non-Java files

        getAllRules.getAllDifference();
        //Write stats.txt file

        RemoveDuplicatesInStats removeDuplicatesInStats;
        removeDuplicatesInStats = new RemoveDuplicatesInStats();
        try {
            removeDuplicatesInStats.removeDup();
        }
        catch (Exception re_dup){
            System.out.println("Remove failed!");
            re_dup.printStackTrace();
        }
        //Remove duplicates in stats.txt

        ParseWithCharniakParser parseWithCharniakParser = new ParseWithCharniakParser();
        parseWithCharniakParser.parseByCharniakParser();
        //Using the CharniakParser to parse the test names

        System.exit(0);

    }
}
