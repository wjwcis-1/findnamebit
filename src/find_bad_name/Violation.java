package find_bad_name;

public class Violation {
    public String testName;
    public String testBody;
    public String violation;
    public Violation(String a, String b, String c){
        this.testName = a;
        this.testBody = b;
        this.violation = c;
    }
    public String getTestName(){
        return testName;
    }
    public String getTestBody(){
            return testBody;
        }
    public String getVioOFRules(){
            return violation;
        }
}
