package find_bad_name;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.item.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static find_bad_name.GetR2R5.ToLower;

public class WordNetSynonyms {
    public List<String> GetSynonyms_Noun(String test_word){
        List<String> result = new ArrayList<>();
        //Todo: Bitbucket users need to change the path
        try {
            Dictionary dict = new Dictionary(new File("/Users/wujianwei/Desktop/Jars/WordNet-3.0/dict"));
            dict.open();
            try {
                IIndexWord indexWord = dict.getIndexWord(test_word, POS.NOUN);
                IWordID iWordID = indexWord.getWordIDs().get(0);
                IWord iWord = dict.getWord(iWordID);
                ISynset iSynset = iWord.getSynset();
                for (IWord w : iSynset.getWords()) {
                    if (!(w.getLemma().equals(ToLower(test_word))) && !(w.getLemma().equals(test_word))) {
                        System.out.println("WordNet: " + w.getLemma() + "\n");
                        result.add(w.getLemma());
                    }
                    else {
                        continue;
                    }
                }
            }
            catch (NullPointerException npe){
                System.out.println("No Sysnonyms found");
            }
            dict.close();
        }
        catch (IOException WN){
            System.out.println("dict not found");
            WN.printStackTrace();
        }
        //WordNet
        return result;
    }
    public List<String> GetSynonyms_Verb(String test_word){
        List<String> result = new ArrayList<>();
        //Todo: Bitbucket users need to change the path
        try {
            Dictionary dict = new Dictionary(new File("/Users/wujianwei/Desktop/Jars/WordNet-3.0/dict"));
            dict.open();
            try {
                IIndexWord indexWord = dict.getIndexWord(test_word, POS.VERB);
                IWordID iWordID = indexWord.getWordIDs().get(0);
                IWord iWord = dict.getWord(iWordID);
                ISynset iSynset = iWord.getSynset();
                for (IWord w : iSynset.getWords()) {
                    if (!(w.getLemma().equals(ToLower(test_word))) && !(w.getLemma().equals(test_word))) {
                        System.out.println("WordNet: " + w.getLemma() + "\n");
                        result.add(w.getLemma());
                    }
                    else {
                        continue;
                    }
                }
            }
            catch (NullPointerException npe1){
                System.out.println("No Sysnonyms found");
            }
            dict.close();
        }
        catch (IOException WN){
            System.out.println("dict not found");
            WN.printStackTrace();
        }
        //WordNet
        return result;
    }

}
