package find_bad_name;

import com.intellij.execution.junit.JUnitUtil;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.PsiShortNamesCache;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static find_bad_name.GetR2R5.ToLower;

public class GetAllRules {
    //TODO: Change All Parameters here
    public int threshold = 2;
    //Default set threshold to 2
    public boolean print_test_body = false;
    //Default Set to false
    private int test_case = 0;
    //Configure the threshold here
    public void getAllDifference(){
        try {
            //TODO: Change the absolute path here if cloned from BitBucket
            File stats = new File("/Users/wujianwei/Desktop/FindName/Output/stats.txt");
            FileWriter fw = new FileWriter(stats, true);
            BufferedWriter output = new BufferedWriter(fw);
            //Setup file writer

            Project p = ProjectManager.getInstance().getOpenProjects()[0];
            output.write("Project: " + p.getName() + "\n");
            GlobalSearchScope scope = GlobalSearchScope.projectScope(p);
            PsiShortNamesCache cache = PsiShortNamesCache.getInstance(p);
            //System.out.println("Tag 1");
            //Get all files which are opened, get all opened windows

            GetNumberList num_list = new GetNumberList();
            String[] number_list = num_list.NumList();
            //get the number list

            //TODO: Use Embedded Prints to check if the plugin can handle the target project
            //Part 1 (may not be able to handle certain projects)
            for (String className : cache.getAllClassNames()) {
                for (PsiClass cls : cache.getClassesByName(className, scope)) {
                    if (JUnitUtil.isTestClass(cls, false, true)) {
                        List<String> put_ins = new ArrayList<>();
                        Pattern tag_1 = Pattern.compile("\\.put[(](.+?)[)]");
                        List<String> new_ins = new ArrayList<>();
                        Pattern tag = Pattern.compile(";(.+?) = new");
                        List<String> add_ins = new ArrayList<>();
                        Pattern tag_2 = Pattern.compile("\\.add[(](.+?)[)]");
                        List<String> color_ins = new ArrayList<>();
                        Pattern tag_3 = Pattern.compile(";(.+?) = Color");
                        //Define matcher for setup -> put/new/add/Color
                        output.write("-Class: "+ cls.getName() +"\n");
                        //OutsideLoop:
                        for (PsiMethod method : cls.getAllMethods()) {
                            Set<String> threshold_counter = new LinkedHashSet<>();
                            //Configure the counter for each test case

                            //For Setup
                            String set_up= "";
                            try {
                                if(method.getName().equals("setup")
                                        || method.getName().equals("setUp")
                                        || method.getName().equals("setUpClass")
                                        || method.getName().equals("SetUp")
                                        || method.getName().equals("SetUpClass")
                                        || method.getName().equals("beforeClass")
                                        || method.getName().equals("before")
                                        || method.getName().equals("After")
                                        || method.getName().equals("AfterClass"))
                                {
                                    set_up = method.getBody().getText();
                                }
                            }
                            catch (Exception setup){
                                System.out.println("Fail to extract setup!");
                            }
                            //Extract setup/before/after class
                            String news = set_up.replaceAll("\n", "");
                            Matcher matcher = tag.matcher(news);
                            while (matcher.find()) {
                                output.write("Setup parts: " + matcher.group(1).trim() + "\n");
                                new_ins.add(matcher.group(1).trim());
                            }
                            String puts = set_up.replaceAll("\n", "");
                            Matcher matcher1 = tag_1.matcher(puts);
                            while (matcher1.find()) {
                                output.write("Setup parts: " + matcher1.group(1).trim() + "\n");
                                put_ins.add(matcher1.group(1).trim());
                            }
                            String adds = set_up.replaceAll("\n", "");
                            Matcher matcher2 = tag_2.matcher(adds);
                            while (matcher2.find()) {
                                output.write("Setup parts: " + matcher2.group(1).trim() + "\n");
                                add_ins.add(matcher2.group(1).trim());
                            }
                            String colors = set_up.replaceAll("\n", "");
                            Matcher matcher3 = tag_3.matcher(colors);
                            while (matcher3.find()) {
                                output.write("Setup parts: " + matcher3.group(1).trim() + "\n");
                                color_ins.add(matcher3.group(1).trim());
                            }
                            //Extract objects by using matches in the setup/before/after class

                            if (JUnitUtil.isTestMethodOrConfig(method)
                                    && (method.getName().contains("test") || method.getName().contains("Test"))) {
                                if (print_test_body){
                                    output.write("Test Name: " + method.getName() + "\n");
                                    output.write("Test Body: " + method.getBody().getText()+ "\n");
                                }
                                test_case = test_case + 1;
                                //Add test_case by 1
                                //TODO: Change or Delete Rule 6 Here
                                String test_name_toSentence = method.getName().replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
                                boolean r1r6_flag = false;
                                if (method.getName().equals("Test")
                                        || method.getName().equals("test")
                                        || method.getName().equals("Test1")
                                        || method.getName().equals("Test2")
                                        || method.getName().equals("test1")
                                        || method.getName().equals("test2")
                                        || method.getName().equals("test_1")
                                        || method.getName().equals("test_2")
                                        || method.getName().equals("Test11")
                                        || method.getName().equals("Test12")
                                        || method.getName().equals("Test21")
                                        || method.getName().equals("Test22")
                                        || method.getName().equals("test_one")
                                        || method.getName().equals("test_two")
                                        ||method.getName().equals("Test_one")
                                        || method.getName().equals("Test_two")) {
                                    output.write("--testname: " +
                                            method.getName() + " -> rule 6 unclear meaning condition(“test1” or No test name)" + "\n");
                                    r1r6_flag = true;
                                    //continue OutsideLoop;
                                }
                                //TODO: Rule 6 unclear meaning
                                System.out.println("Test Case: " + method.getName());
                                String[] test_name_words = method.getName().split("(?=\\p{Upper})");
                                //TODO: Change or Delete Rule 3 Here
                                for (int k0 = 0; k0 < test_name_words.length; k0++) {
                                    Properties prop_1 = new Properties();
                                    prop_1.setProperty("annotators", "tokenize, ssplit, pos, lemma, parse");
                                    StanfordCoreNLP pipeline = new StanfordCoreNLP(prop_1);
                                    Annotation parsed_name = new Annotation(test_name_toSentence);
                                    pipeline.annotate(parsed_name);
                                    List<CoreMap> ss1 = parsed_name.get(CoreAnnotations.SentencesAnnotation.class);
                                    for(CoreMap s1 : ss1){
                                        for (CoreLabel token : s1.get(CoreAnnotations.TokensAnnotation.class)) {
                                            String pos_1 = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                                            //System.out.println("Test Body:"+method.getBody().getText());
                                            if (pos_1.equals("DT")||pos_1.equals("PDT")||pos_1.equals("CD")) {
                                                int rule_3_counter = 0;
                                                for (String new1 : new_ins){
                                                    if (method.getBody().getText().contains(new1)){
                                                        rule_3_counter += 1;
                                                        //continue OutsideLoop;
                                                    }
                                                }
                                                for (String put1 : put_ins){
                                                    if (method.getBody().getText().contains(put1)){
                                                        rule_3_counter += 1;
                                                        //continue OutsideLoop;
                                                    }
                                                }
                                                for (String add1 : add_ins){
                                                    if (method.getBody().getText().contains(add1)){
                                                        rule_3_counter += 1;
                                                        //continue OutsideLoop;
                                                    }
                                                }
                                                for (String color1 : color_ins){
                                                    if (method.getBody().getText().contains(color1)){
                                                        rule_3_counter += 1;
                                                        //continue OutsideLoop;
                                                    }
                                                }
                                                //The test case have all news,puts,adds,colors
                                                if (new_ins.isEmpty() && put_ins.isEmpty() && add_ins.isEmpty() && color_ins.isEmpty() || rule_3_counter > 0) {
                                                    //Do nothing
                                                    //continue  OutsideLoop;
                                                } else {
                                                    threshold_counter.add("--testname: " +
                                                            method.getName() + "-> rule 3 testname with determiner words or cardinal numbers");
                                                }
                                                //continue OutsideLoop;
                                            }
                                        }
                                    }
                                }
                                //TODO: Rule 3 testname with pre-determiner words

                                //TODO: Change or Delete Rule 4 Here
                                for (int k1 = 0; k1 <test_name_words.length; k1++){
                                    for (int k2 = 0; k2 < number_list.length; k2++) {
                                        System.out.println("NumberList: "+test_name_words[k1]+"-->"+number_list[k2]);
                                        if (test_name_words[k1].equals(number_list[k2]) || ToLower(test_name_words[k1]).equals(number_list[k2])) {
                                            threshold_counter.add("--testname: " +
                                                    method.getName() + "-> rule 4 testname with numbered parts->" + number_list[k2]);
                                            //continue OutsideLoop;
                                        }
                                    }
                                }
                                //TODO: Rule 4 testname with numbers

                                //TODO: Change or Delete Rule 2&5 Here
                                String test_name = method.getName();
                                String test_body = method.getBody().getText();
                                GetR2R5 getR2R5 = new GetR2R5();
                                if (getR2R5.getDifference(test_name,test_body) != null) {
                                    for (String r2r5 : getR2R5.getDifference(test_name, test_body)) {
                                        threshold_counter.add(r2r5);
                                    }
                                }
                                //TODO: Add results from R2R5 if not null

                                //TODO: Change or Delete Rule 1 Here
                                int count = 0;
                                String[] test_name_words_1 = test_name.split("(?=\\p{Upper})");
                                String[] test_name_words_2 = test_name.split("_");
                                for (int k4 = 1; k4 < test_name_words_1.length; k4++) {
                                    if (test_body.contains(test_name_words_1[k4])
                                            || test_body.contains(ToLower(test_name_words_1[k4]))) {
                                        output.write("Capital: " + test_name_words_1[k4] + "\n");
                                        count += 1;
                                    }
                                }
                                for (int k4 = 1; k4 < test_name_words_2.length; k4++) {
                                    if (test_body.contains(test_name_words_2[k4])
                                            || test_body.contains(ToLower(test_name_words_2[k4]))) {
                                        output.write("Underscore: " + test_name_words_2[k4] + "\n");
                                        count += 1;
                                    }
                                }
                                if (count == 0) {
                                    output.write("--testname: " + test_name +
                                            "-> rule 1 None of words in the testname are showing in the body" + "\n");
                                    r1r6_flag = true;
                                    //continue OutsideLoop;
                                }
                                //TODO: Rule 1 None of words in the testname are showing in the body
                                for (String sample : threshold_counter){
                                    output.write("threshold_counter: " + sample +"\n");
                                }
                                //If a name violates Rule 1 or Rule 6, it will NOT write the threshold set
                                if (threshold_counter.size() >= threshold && !r1r6_flag){
                                    output.write(threshold_counter.stream().findFirst().get() + "\n");
                                }
                                //Check the threshold_counter
                            }
                        }
                        //End of Loop
                    }
                    else{
                        System.out.println(cls.getName()+" is not a Junit test");
                    }
                }
            }

            //Part 2 Test Code, using VirtualFile
            //output.write("test_case-> "+test_case+"\n");
            if (test_case == 0){
                final Collection<VirtualFile> all_files = FileTypeIndex.getFiles(JavaFileType.INSTANCE,GlobalSearchScope.projectScope(p));
                for (VirtualFile virtualFile : all_files) {
                    PsiFile file = PsiManager.getInstance(p).findFile(virtualFile);
                    if (file instanceof PsiJavaFile &&
                            (file.getName().contains("Test")
                                    || file.getName().contains("test"))){
                        for (PsiClass psiClass : ((PsiJavaFile) file).getClasses()){
                            output.write("Class: " + psiClass.getName() + "\n");
                            //Define Setup
                            List<String> put_ins = new ArrayList<>();
                            Pattern tag_1 = Pattern.compile("\\.put[(](.+?)[)]");
                            List<String> new_ins = new ArrayList<>();
                            Pattern tag = Pattern.compile(";(.+?) = new");
                            List<String> add_ins = new ArrayList<>();
                            Pattern tag_2 = Pattern.compile("\\.add[(](.+?)[)]");
                            List<String> color_ins = new ArrayList<>();
                            Pattern tag_3 = Pattern.compile(";(.+?) = Color");
                            //Define matcher for setup -> put/new/add/Color

                            for (PsiMethod method : psiClass.getAllMethods()){
                                //System.out.println("Method: " + method.getName());
                                String set_up = "";
                                try {
                                    if (method.getName().equals("setup")
                                            || method.getName().equals("setUp")
                                            || method.getName().equals("setUpClass")
                                            || method.getName().equals("SetUp")
                                            || method.getName().equals("SetUpClass")
                                            || method.getName().equals("beforeClass")
                                            || method.getName().equals("before")
                                            || method.getName().equals("After")
                                            || method.getName().equals("AfterClass")) {
                                        set_up = method.getBody().getText();
                                        output.write("Setup body: " + set_up.replaceAll("\n", "").trim() + "\n");
                                    }
                                } catch (Exception setup) {
                                    System.out.println("Fail to extract setup!");
                                }
                                //Extract setup class
                                String news = set_up.replaceAll("\n", "");
                                Matcher matcher = tag.matcher(news);
                                while (matcher.find()) {
                                    output.write("Setup parts: " + matcher.group(1).trim() + "\n");
                                    new_ins.add(matcher.group(1).trim());
                                }
                                String puts = set_up.replaceAll("\n", "");
                                Matcher matcher1 = tag_1.matcher(puts);
                                while (matcher1.find()) {
                                    output.write("Setup parts: " + matcher1.group(1).trim() + "\n");
                                    put_ins.add(matcher1.group(1).trim());
                                }
                                String adds = set_up.replaceAll("\n", "");
                                Matcher matcher2 = tag_2.matcher(adds);
                                while (matcher2.find()) {
                                    output.write("Setup parts: " + matcher2.group(1).trim() + "\n");
                                    add_ins.add(matcher2.group(1).trim());
                                }
                                String colors = set_up.replaceAll("\n", "");
                                Matcher matcher3 = tag_3.matcher(colors);
                                while (matcher3.find()) {
                                    output.write("Setup parts: " + matcher3.group(1).trim() + "\n");
                                    color_ins.add(matcher3.group(1).trim());
                                }
                                //Extract objects by using matches in the setup/before/after class

                                if (method.getName().contains("test")
                                        || method.getName().contains("Test")) {
                                    if (print_test_body){
                                        output.write("**Test Name: " + method.getName()+ "\n");
                                        output.write("**Test Body: " + method.getBody().getText()+ "\n");
                                    }
                                    //JunitUtil will not work here -> some test are NOT standard Junit tests
                                    try {
                                        boolean r1r6_flag_1 = false;
                                        Set<String> threshold_counter = new LinkedHashSet<>();
                                        //Configure the counter for each test case
                                        if (method.getName().equals("Test")
                                                || method.getName().equals("test")
                                                || method.getName().equals("Test1")
                                                || method.getName().equals("Test2")
                                                || method.getName().equals("test1")
                                                || method.getName().equals("test2")
                                                || method.getName().equals("test_1")
                                                || method.getName().equals("test_2")
                                                || method.getName().equals("Test11")
                                                || method.getName().equals("Test12")
                                                || method.getName().equals("Test21")
                                                || method.getName().equals("Test22")
                                                || method.getName().equals("test_one")
                                                || method.getName().equals("test_two")
                                                || method.getName().equals("Test_one")
                                                || method.getName().equals("Test_two")) {
                                            output.write("--testname: " + method.getName() + " -> rule 6 unclear meaning condition(“test1” or No test name)" + "\n");
                                            r1r6_flag_1 = true;
                                        }
                                        //rule 6 unclear meaning
                                        //For Setup
                                        System.out.println("Test Case:" + method.getName());
                                        //Confirm test class
                                        String test_name_toSentence = method.getName().replaceAll("(\\p{Ll})(\\p{Lu})", "$1 $2");
                                        String[] test_name_words = method.getName().split("(?=\\p{Upper})");
                                        //Convert test name to sentence
                                        for (int k0 = 0; k0 < test_name_words.length; k0++) {
                                            Properties prop_1 = new Properties();
                                            prop_1.setProperty("annotators", "tokenize, ssplit, pos, lemma, parse");
                                            StanfordCoreNLP pipeline = new StanfordCoreNLP(prop_1);
                                            Annotation parsed_name = new Annotation(test_name_toSentence);
                                            pipeline.annotate(parsed_name);
                                            List<CoreMap> ss1 = parsed_name.get(CoreAnnotations.SentencesAnnotation.class);
                                            for (CoreMap s1 : ss1) {
                                                for (CoreLabel token : s1.get(CoreAnnotations.TokensAnnotation.class)) {
                                                    String pos_1 = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                                                    //System.out.println("Test Body:" + method.getBody().getText());
                                                    System.out.println("Parsed name: " + token.toString() + "-" +pos_1);
                                                    if (pos_1.equals("DT") || pos_1.equals("PDT") || pos_1.equals("CD")) {
                                                        int rule_3_counter = 0;
                                                        for (String new1 : new_ins) {
                                                            if (method.getBody().getText().contains(new1)) {
                                                                rule_3_counter += 1;
                                                            }
                                                        }
                                                        for (String put1 : put_ins) {
                                                            if (method.getBody().getText().contains(put1)) {
                                                                rule_3_counter += 1;
                                                            }
                                                        }
                                                        for (String add1 : add_ins) {
                                                            if (method.getBody().getText().contains(add1)) {
                                                                rule_3_counter += 1;
                                                            }
                                                        }
                                                        for (String color1 : color_ins) {
                                                            if (method.getBody().getText().contains(color1)) {
                                                                rule_3_counter += 1;
                                                            }
                                                        }
                                                        //The test case have all news,puts,adds,colors
                                                        if (new_ins.isEmpty()&&put_ins.isEmpty()&&add_ins.isEmpty()&&color_ins.isEmpty()) {
                                                            //continue  OutsideLoop;
                                                        }
                                                        else if (rule_3_counter > 0){
                                                            //Do nothing
                                                        }
                                                        else {
                                                            threshold_counter.add("--testname: " +
                                                                    method.getName() + "-> rule 3 testname with determiner words or cardinal numbers");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // rule 3 testname with pre-determiner words
                                        for (int k2 = 0, k0 = 0; k2 < number_list.length && k0 < test_name_words.length; k2++, k0++) {
                                            if (test_name_words[k0].equals(number_list[k2]) || ToLower(test_name_words[k0]).equals(number_list[k2])) {
                                                threshold_counter.add("--testname: " +
                                                        method.getName() + "-> rule 4 testname with numbered parts");
                                            }
                                        }
                                        // rule 4 testname with numbers
                                        String test_name = method.getName();
                                        String test_body = method.getBody().getText();
                                        GetR2R5 getR2R5 = new GetR2R5();
                                        if (getR2R5.getDifference(test_name,test_body) != null) {
                                            for (String r2r5 : getR2R5.getDifference(test_name, test_body)) {
                                                threshold_counter.add(r2r5);
                                            }
                                        }
                                        //Add results from R2R5 if not null
                                        int count = 0;
                                        String[] test_name_words_1 = test_name.split("(?=\\p{Upper})");
                                        String[] test_name_words_2 = test_name.split("_");
                                        for (int k4 = 1; k4 < test_name_words_1.length; k4++) {
                                            if (test_body.contains(test_name_words_1[k4]) || test_body.contains(ToLower(test_name_words_1[k4]))) {
                                                count += 1;
                                            }
                                        }
                                        for (int k4 = 1; k4 < test_name_words_2.length; k4++) {
                                            if (method.getBody().getText().contains(test_name_words_2[k4])
                                                    || method.getBody().getText().contains(ToLower(test_name_words_2[k4]))) {
                                                output.write("Underscore: " + test_name_words_2[k4] + "\n");
                                                count += 1;
                                            }
                                        }
                                        if (count == 0) {
                                            output.write("--testname: " + test_name +
                                                    "-> rule 1 None of words in the testname are showing in the body" + "\n");
                                            r1r6_flag_1 = true;
                                            //continue OutsideLoop;
                                        }
                                        //Rule 1 None of words in the testname are showing in the body

                                        for (String sample : threshold_counter){
                                            output.write("threshold_counter: " + sample +"\n");
                                        }
                                        if (threshold_counter.size() >= threshold && !r1r6_flag_1){
                                            output.write(threshold_counter.stream().findFirst().get() + "\n");
                                        }
                                    } catch (Exception ee1) {
                                        System.out.println("Output file can NOT be found!-1");
                                    }
                                }
                            }
                            //End of methods
                        }
                    }
                }
            }
            //Else -> Do nothing
            output.write( "* End of Project (" + p.getName() +") *" + "\n");
            output.close();
        }
        catch (IOException a) {
            System.out.println("Stats.txt can NOT be found");
        }
    }
}
